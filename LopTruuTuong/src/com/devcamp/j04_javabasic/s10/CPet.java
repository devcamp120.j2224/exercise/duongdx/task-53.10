package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class CPet extends CAnimal {
	protected int age;
	protected String name;
	@Override
	public void animalSound() {
		// TODO Auto-generated method stub
		System.out.println("Pet sound...");
	}
	@Override
	public void eat() {
		System.out.println("Pet eating...");
	};
	protected void print() {	
	}
	protected void  play() {	
	}
	public static void main(String[] args) {
		CPet myPets = new CPet();
		myPets.name = "My Eagle";
		myPets.animclass = AnimalClass.mammals;
		myPets.eat();
		myPets.animalSound();
		myPets.print();
		myPets.play();

		CPet myFish = new CFish();
		myFish.name = "My Eagle";
		myFish.animclass = AnimalClass.mammals;
		myFish.eat();
		myFish.animalSound();
		myFish.print();
		myFish.play();
		((CFish)myFish).swim();

		CBird myBird = new CBird();
		myBird.name = "My Cho";
		myBird.animclass = AnimalClass.mammals;
		myBird.eat();
		myBird.animalSound();
		myBird.print();
		myBird.play();
		myBird.fly();
		
		CAnimal nameA2 = new CFish();
		CPet name2 = new CPet();

		CPerson namePerson = new CPerson();
		namePerson.setAge(27);
		namePerson.setId(2);
		namePerson.setFirstname("Duong");
		namePerson.setLastname("Dao Xuan");
		namePerson.setPets("a","b","c");
		ArrayList<CPet> petsList = new ArrayList();
		petsList.add(name2);
		petsList.add((CPet) nameA2);
		namePerson.setPets(petsList);
		System.out.println(namePerson);
	}
}
