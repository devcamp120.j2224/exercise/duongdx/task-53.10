package com.devcamp.j04_javabasic.s10;
import java.util.ArrayList;
public class CPerson extends CAnimal{
    @Override
    public void animalSound() {
        System.out.println("person speaking");
    }
    public CPerson(){
        super();
    }
    /**
     * khởi tạo CPerson với đầy đủ các tham số
     *  @param id
     *  @param age
     *  @param firstname
     *  @param lastname
     *  @param pets
     */
     public CPerson(int id, int age , String firstname , String lastname , ArrayList<CPet> pets){
        super();
        this.id = id ;
        this.age = age ;
        this.firstname = firstname ;
        this.lastname = lastname ;
        this.pets = pets ;
     }
     private int id ;
     private int age ;
     private String firstname ;
     private String lastname ;
     private ArrayList<CPet> pets ;

     /**
      * @return the id
      */
      public int getId(){
        return id;
      }

    /**
    * @param id  the id to set
    */
    public void setId(int id){
        this.id = id ;
    }
    /**
    * @return  the age
    */
    public int getAge(){
        return age;
      }
    /**
    * @param age  the age to set
    */
    public void setAge(int age){
        this.age = age ;
    }
    /**
    * @return  the firstname
    */
    public String getFirstname(){
        return firstname;
      }
    /**
    * @param firstname  the firstname to set
    */
    public void setFirstname(String firstname){
        this.firstname = firstname ;
    }
    /**
    * @return  the lastname
    */
    public String getLastname(){
        return lastname;
      }
    /**
    * @param lastname  the lastname to set
    */
    public void setLastname(String lastname){
        this.lastname = lastname ;
    }
    /**
    * @return  the pets
    */
    public ArrayList<CPet> getPets(){
        return pets;
      }
    /**
    * @param pets  the pets to set
    */
    public void setPets(ArrayList<CPet> pets1){
        this.pets = pets1 ;
    }
    @Override
    public String toString(){
        return "CPerson {\"id\":" + this.id + ", age= " + this.age + ", firstname= " + this.firstname + ", lastname= " + this.lastname
        + ", pets= " + this.pets  + "}";
    }
    public void setPets(String string, String string2, String string3) {
    }
}
